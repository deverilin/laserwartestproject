﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaserWar.Base;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using LaserWar.Interfaces;
using LaserWar.VisualElements;
using System.IO;

namespace LaserWar
{
    class MainWindowViewModel : BaseViewModel
    {

        /// <summary>
        /// Представление части окна (переключается в зависимости от нажатой кнопки меню)
        /// </summary>
        private IVisualElement currentWindowElement;

        public IVisualElement CurrentWindowElement
        {
            get
            {
                return currentWindowElement;
            }

            set
            {
                currentWindowElement = value;
                OnPropertyChanged("CurrentWindowElement");
            }
        }

        /// <summary>
        /// Коллекция визуальных компонентов.
        /// </summary>
        public ObservableCollection<IVisualElement> WindowElements { get; set; }

        public MainWindowViewModel()
        {
            WindowElements = new ObservableCollection<IVisualElement>();
            WindowElements.Add(new FileUploadVisualElement());
            WindowElements.Add(new SoundsVisualElement());
            WindowElements.Add(new DBEditorVisualElement());

            Directory.CreateDirectory("Temp");
        }

        ~MainWindowViewModel()
        {
            Directory.Delete("Temp", true);
        }
    }
}
