﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace LaserWar.Interfaces
{
    interface IVisualElement
    {
        UserControl GetControl { get; }
        BitmapImage GetIcon { get; }
    }
}
