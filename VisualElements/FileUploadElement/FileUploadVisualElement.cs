﻿using LaserWar.Base;
using LaserWar.DataBase;
using LaserWar.Interfaces;
using LaserWar.Resources;
using LaserWar.VisualElements.DBEditorElement;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Xml;

namespace LaserWar.VisualElements
{
    class FileUploadVisualElement : BaseViewModel, IVisualElement
    {
        public FileUploadVisualElement()
        {
            modelDB = DataBaseSingleton.ModelDB;
        }

        #region PRIVATE PROPERTIES
        private DataBaseModel modelDB;

        /// <summary>
        /// URL JSON-файла конфигурации.
        /// </summary>
        private string confUrl;

        /// <summary>
        /// Статус - загружен ли файл.
        /// </summary>
        private string fileUploadStatus;

        /// <summary>
        /// Окно ожидания.
        /// </summary>
        private PleaseWaitWindow WaitWindow;

        #endregion

        #region EVENT HANDLERS

        private void GoDownloadJSONFile(object sender, DoWorkEventArgs e)
        {
            string jsonRoot = "";
            e.Result = "";

            HttpWebRequest request;

            try
            {
                request = (HttpWebRequest)WebRequest.Create(ConfUrl);
                request.Method = "GET";
                request.Timeout = 3000;

                HttpWebResponse responce = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(responce.GetResponseStream());
                jsonRoot = reader.ReadToEnd();
                reader.Close();
            }

            catch(Exception exc)
            {
                e.Result = "Не удалось загрузить файл!";
                return;
            }

            JObject jDoc;
            JToken jGames;
            JToken jSounds;

            try
            {
                jDoc = JObject.Parse(jsonRoot);
                jGames = jDoc["games"];
                jSounds = jDoc["sounds"];
            }
            catch (Exception exc)
            {
                e.Result = "Не удалось обработать файл!";
                return;
            }

            /*
             * При работе с базой данных подразумеваем следующее:
             * 1) Задачи накопления данных не стоит, т.е. загружая каждый раз новый набор XML-файлов, очищаем БД. Добавлять или
             * изменять существующие записи в таблицах загрузкой новых файлов мы не можем.
             * 2) Каждая игра имеет уникальный набор полей название-дата. Мы не можем начать проводить две игры
             * с одинаковым названием в одну и ту же секунду.
             * 3) Комады могут иметь одинаковые названия.
             * 4) Игроки не могут иметь одинаковых имен.
             */

            modelDB.Games.Include("Teams").Load();
            modelDB.Teams.Include("Players").Load();

            //Удаляем существующие записи в БД.
            modelDB.Games.RemoveRange(modelDB.Games.Include("Teams"));
            modelDB.Teams.RemoveRange(modelDB.Teams.Include("Players"));

            //Скачиваем все XML файлы, указанные в JSON.
            foreach (JToken jGame in jGames)
            {
                HttpWebRequest xmlRequest = (HttpWebRequest)WebRequest.Create(jGame.Value<string>("url"));
                request.Method = "GET";
                request.Timeout = 3000;

                XmlDocument xGameDoc = new XmlDocument();

                try
                {
                    HttpWebResponse xmlResponce = (HttpWebResponse)xmlRequest.GetResponse();
                    StreamReader xmlStream = new StreamReader(xmlResponce.GetResponseStream());

                    xGameDoc.InnerXml = xmlStream.ReadToEnd();
                    xmlStream.Close();
                }
                catch (Exception anyExcept)
                {
                    e.Result = "Не удалось обработать файл: ошибка загрузки XML!";
                    return;
                }

                try
                {
                    //Парсим XML файл.
                    XmlElement xCurrGame = xGameDoc.DocumentElement;

                    Game newGame = new Game { Name = xCurrGame.GetAttribute("name"), Date = int.Parse(xCurrGame.GetAttribute("date")) };

                    //Проверяем - существует ли такая запись в БД. Если существует, не добавляем ее и переходим к следующией итерации.
                    Game matchGame = modelDB.Games.Local.ToList().Find(x => x.Name == newGame.Name && x.Date == newGame.Date);
                    if (matchGame != null) continue;

                    modelDB.Games.Add(newGame);

                    //Проходим все команды в данной игре.
                    foreach (XmlElement xTeam in xCurrGame.ChildNodes)
                    {
                        Team newTeam = new Team { Name = xTeam.GetAttribute("name") };

                        newGame.Teams.Add(newTeam);

                        //Проходим всех игроков в данной команде.
                        foreach (XmlElement xPlayer in xTeam.ChildNodes)
                        {
                            Player newPlayer = new Player
                            {
                                Name = xPlayer.GetAttribute("name"),
                                Rating = int.Parse(xPlayer.GetAttribute("rating")),
                                Accuracy = float.Parse((xPlayer.GetAttribute("accuracy").Replace(".", ","))),
                                Shots = int.Parse(xPlayer.GetAttribute("shots"))
                            };

                            //Проверяем - существует ли уже игрок с таким именем. Если существует, берем его, а не создаем нового.
                            Player matchPlayer = modelDB.Players.Local.ToList().Find(x => x.Name == newPlayer.Name);
                            if (matchPlayer == null)
                            {
                                modelDB.Players.Add(newPlayer);
                            }
                            else
                            {
                                newPlayer = matchPlayer;
                            }

                            newTeam.Players.Add(newPlayer);
                        }

                    }

                }
                catch(Exception exc)
                {
                    e.Result = "Не удалось обработать файл!";
                    return;
                }

            }

            //Парсим звуки
            modelDB.Sounds.Load();
            List<Sound> lSounds = modelDB.Sounds.ToList();
            foreach (JToken jSound in jSounds)
            {
                Sound sound = new Sound()
                {
                    Name = jSound.Value<string>("name"),
                    Url = jSound.Value<string>("url"),
                    Size = jSound.Value<int>("size")
                };

                Sound matchSound = lSounds.Find(x => x.Name == sound.Name || x.Url == sound.Url);

                if (matchSound != null)
                {
                    Sound modSound = modelDB.Sounds.Find(matchSound.Id);
                    if (modSound == null) continue;
                    modSound.Name = sound.Name;
                    modSound.Url = sound.Url;
                    modSound.Size = sound.Size;
                    modelDB.Entry(modSound).State = EntityState.Modified;
                }
                else
                {
                    modelDB.Sounds.Add(sound);
                }

            }

            modelDB.SaveChanges();

        }

        private void JSONDownloaded(object sender, RunWorkerCompletedEventArgs e)
        {
            WaitWindow.Close();

            string res = (string)e.Result;
            if (res != "")
            {
                new ErrorWindow(res).ShowDialog();
                return;
            }

            DataBaseSingleton.DatabaseUpdated(this, new EventArgs());

            FileUploadStatus = "Файл успешно загружен!";
        }

        #endregion

        #region PUBLIC PROPERTIES

        public string ConfUrl
        {
            get
            {
                return confUrl;
            }

            set
            {
                confUrl = value;
                if (FileUploadStatus != "") FileUploadStatus = "";
                OnPropertyChanged("ConfUrl");
            }
        }

        public string FileUploadStatus
        {
            get
            {
                return fileUploadStatus;
            }
            set
            {
                fileUploadStatus = value;
                OnPropertyChanged("FileUploadStatus");
            }
        }

        #endregion

        #region COMMANDS

        /// <summary>
        /// Команда загрузки файла JSON.
        /// </summary>
        public ICommand DownloadConfFile
        {
            get
            {
                return new RelayCommand(o =>
                {
                    if (ConfUrl == null) return;
                    BackgroundWorker downloadWorker = new BackgroundWorker();
                    downloadWorker.DoWork += GoDownloadJSONFile;
                    downloadWorker.RunWorkerCompleted += JSONDownloaded;

                    WaitWindow = new PleaseWaitWindow();
                    WaitWindow.Show();

                    downloadWorker.RunWorkerAsync();

                });
            }
        }

        #endregion

        #region INTERFACE IMPLEMENTATION

        public BitmapImage GetIcon
        {
            get { return new BitmapImage(new Uri("pack://application:,,,/Resources/download.png")); }
        }

        public UserControl GetControl
        {
            get { return new FileUploadUserControl(this); }
        }

        #endregion
    }
}
