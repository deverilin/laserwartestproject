﻿using LaserWar.Base;
using LaserWar.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LaserWar.VisualElements
{
    /// <summary>
    /// Логика взаимодействия для SoundsUserControl.xaml
    /// </summary>
    public partial class SoundsUserControl : UserControl
    {
        public SoundsUserControl(BaseViewModel ViewModel)
        {
            InitializeComponent();
            DataContext = ViewModel;
        }
    }
}
