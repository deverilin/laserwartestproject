﻿using LaserWar.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LaserWar.VisualElements.SoundsElement
{
    /// <summary>
    /// Логика взаимодействия для SoundWrapperDownloadUserControl.xaml
    /// </summary>
    public partial class SoundWrapperDownloadUserControl : UserControl
    {
        public SoundWrapperDownloadUserControl(BaseViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
