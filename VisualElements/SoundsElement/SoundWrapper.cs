﻿using LaserWar.Base;
using LaserWar.DataBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace LaserWar.VisualElements.SoundsElement
{
    class SoundWrapper : BaseViewModel
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="inPlayer">Медиа-плеер звуков. Общий для всех звуков.</param>
        /// <param name="inSound">Объект звука-сущности для БД.</param>
        public SoundWrapper(MediaPlayer inPlayer, Sound inSound)
        {
            sound = inSound;
            soundPlayer = inPlayer;

            soundPlayer.MediaEnded += SoundPlay_Stoped;
            soundPlayer.MediaOpened += SoundFile_Opened;

            DownloadClient = new WebClient();
            DownloadClient.DownloadProgressChanged += ChangeDownloadingProgress;
            DownloadClient.DownloadFileCompleted += File_Uploaded;
            DownloadedFileAbsolutePath = "";

            downloadPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/download_sound.png"));
            downloadingPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/downloading_sound.png"));
            downloadedPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/downloaded_sound.png"));

            currentDownloadPicture = downloadPicture;

            playBtnPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/play.png"));
            stopBtnPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/stop.png"));

            currentPlayStopBtnPicture = playBtnPicture;
            currentPlayStopBtnCommand = PlaySound;

            VisibilityOfDownloadingStack = Visibility.Collapsed;
            VisibilityOfPlayingStack = Visibility.Collapsed;

            VisibilityOfUploadedLabel = Visibility.Collapsed;

            SoundPlayingTimer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 10) };
            SoundPlayingTimer.Tick += SoundPlayingTimer_Ticked;

            playingUserControl = new SoundWrapperPlayingUserControl(this);
        }

        #region PRIVATE PROPERTIES

        /// <summary>
        /// Звук. Сущность для БД.
        /// </summary>
        private Sound sound;

        /// <summary>
        /// Ссылка на медиа плеер (общий для всех элементов).
        /// </summary>
        private MediaPlayer soundPlayer;

        /// <summary>
        /// Абсолютный путь к скачанному файлу.
        /// </summary>
        private string DownloadedFileAbsolutePath;

        /// <summary>
        /// Процент скачивания файла звука.
        /// </summary>
        private int downloadPercents;

        /// <summary>
        /// Видимость элементов прогресса проигрывания.
        /// </summary>
        private Visibility visibilityOfPlayingStack;

        /// <summary>
        /// Видимость элементов прогресса скачивания.
        /// </summary>
        private Visibility visibilityOfDownloadingStack;

        /// <summary>
        /// Видимость надписи "Файл загружен".
        /// </summary>
        private Visibility visibilityOfUploadedLabel;

        /// <summary>
        /// Таймер обновления прогресс-бара проигрывания звука.
        /// </summary>
        private DispatcherTimer SoundPlayingTimer;

        //Иконки кнопки "Скачать".
        /// <summary>
        /// Иконка "скачать звук".
        /// </summary>
        private BitmapImage downloadPicture;

        /// <summary>
        /// Иконка "звук скачан".
        /// </summary>
        private BitmapImage downloadedPicture;

        /// <summary>
        /// Иконка "звук скачивается".
        /// </summary>
        private BitmapImage downloadingPicture;

        /// <summary>
        /// Текущая иконка, прибитая к кнопке в представлении.
        /// </summary>
        private BitmapImage currentDownloadPicture;

        //Иконки кнопки "Проиграть звук".
        /// <summary>
        /// Иконка "Проиграть звук".
        /// </summary>
        private BitmapImage playBtnPicture;

        /// <summary>
        /// Иконка "Остановить проигрывание звука".
        /// </summary>
        private BitmapImage stopBtnPicture;

        /// <summary>
        /// Текущая иконка, прибитая к кнопке на представлении.
        /// </summary>
        private BitmapImage currentPlayStopBtnPicture;

        #endregion

        #region PUBLIC PROPERTIES

        /// <summary>
        /// Объект для скачивания звуков.
        /// </summary>
        public WebClient DownloadClient { get; set; }

        /// <summary>
        /// Принимает и возвращает процент скачанного файла <see cref="downloadPercents"/>.
        /// </summary>
        public int DownloadPercents
        {
            get
            {
                return downloadPercents;
            }

            set
            {
                downloadPercents = value;
                OnPropertyChanged("DownloadPercents");
            }
        }

        public MediaPlayer SoundPlayer
        {
            get
            {
                return soundPlayer;
            }
        }

        public string PlayingCurrentPosition
        {
            get
            {
                return soundPlayer.Position.ToString(@"mm\:ss");
            }
        }

        public Visibility VisibilityOfPlayingStack
        {
            get
            {
                return visibilityOfPlayingStack;
            }

            set
            {
                visibilityOfPlayingStack = value;
                OnPropertyChanged("VisibilityOfPlayingStack");
            }
        }

        public Visibility VisibilityOfDownloadingStack
        {
            get
            {
                return visibilityOfDownloadingStack;
            }

            set
            {
                visibilityOfDownloadingStack = value;
                OnPropertyChanged("VisibilityOfDownloadingStack");
            }
        }

        public Visibility VisibilityOfUploadedLabel
        {
            get
            {
                return visibilityOfUploadedLabel;
            }

            set
            {
                visibilityOfUploadedLabel = value;
                OnPropertyChanged("VisibilityOfUploadedLabel");
            }
        }
        
        public double SoundDuration
        {
            get
            {
                if (soundPlayer.NaturalDuration.HasTimeSpan)
                {
                    return soundPlayer.NaturalDuration.TimeSpan.Ticks;
                }
                else
                {
                    return 0;
                }
            }
        }

        public double SoundPosition
        {
            get
            {
                return soundPlayer.Position.Ticks;
            }
        }

        public BitmapImage CurrentDownloadPicture
        {
            get
            {
                return currentDownloadPicture;
            }

            set
            {
                currentDownloadPicture = value;
                OnPropertyChanged("CurrentDownloadPicture");
            }
        }

        public BitmapImage CurrentPlayStopBtnPicture
        {
            get
            {
                return currentPlayStopBtnPicture;
            }

            set
            {
                currentPlayStopBtnPicture = value;
                OnPropertyChanged("CurrentPlayStopBtnPicture");
            }
        }

        public string Name
        {
            get
            {
                return sound.Name;
            }

        }

        public string Size
        {
            get
            {
                return String.Format("{0} Kb", sound.Size / 1024);
            }
        }

        public UserControl DownloadControl
        {
            get { return new SoundWrapperDownloadUserControl(this); }
        }

        private SoundWrapperPlayingUserControl playingUserControl;

        public UserControl PlayingControl
        {
            get { return playingUserControl; }
        }

        #endregion

        #region EVENTS HANDLERS

        /// <summary>
        /// Обработчик события изменения количества процентов скачанного файла.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeDownloadingProgress(object sender, DownloadProgressChangedEventArgs e)
        {
            DownloadPercents = e.ProgressPercentage;
        }

        private bool IsStopEventSensitive;

        /// <summary>
        /// Обработчик события окончания проигрывания файла.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SoundPlay_Stoped(object sender, EventArgs e)
        {
            CurrentPlayStopBtnPicture = playBtnPicture;
            CurrentPlayStopBtnCommand = PlaySound;

            if (VisibilityOfPlayingStack == Visibility.Visible)
            {
                VisibilityOfPlayingStack = Visibility.Collapsed;
            }

            SoundPlayingTimer.Stop();
        }

        private void SoundFile_Opened(object sender, EventArgs e)
        {
            if (!IsStopEventSensitive)
            {
                IsStopEventSensitive = true;
                return;
            }

            SoundPlay_Stoped(this, EventArgs.Empty);
        }

        private void SoundPlayingTimer_Ticked(object sender, EventArgs e)
        {
            OnPropertyChanged("SoundDuration");
            OnPropertyChanged("SoundPosition");
            OnPropertyChanged("PlayingCurrentPosition");
        }

        private void File_Uploaded(object sender, EventArgs e)
        {
            VisibilityOfDownloadingStack = Visibility.Collapsed;
            VisibilityOfUploadedLabel = Visibility.Visible;
            CurrentDownloadPicture = downloadedPicture;
        }

        #endregion

        #region COMMANDS

        private ICommand currentPlayStopBtnCommand;
        public ICommand CurrentPlayStopBtnCommand
        {
            get
            {
                return currentPlayStopBtnCommand;
            }

            set
            {
                currentPlayStopBtnCommand = value;
                OnPropertyChanged("CurrentPlayStopBtnCommand");
            }
        }

        public ICommand PlaySound
        {
            get
            {
                return new RelayCommand(o =>
                {
                    if (DownloadedFileAbsolutePath == "") return;

                    IsStopEventSensitive = false;

                    soundPlayer.Open(new Uri(DownloadedFileAbsolutePath));

                    VisibilityOfPlayingStack = Visibility.Visible;
                    CurrentPlayStopBtnPicture = stopBtnPicture;
                    CurrentPlayStopBtnCommand = StopSound;

                    SoundPlayingTimer.Start();

                    soundPlayer.Play();
                });
            }
        }

        public ICommand StopSound
        {
            get
            {
                return new RelayCommand(o =>
                {
                    SoundPlayer.Stop();
                    CurrentPlayStopBtnPicture = playBtnPicture;
                    CurrentPlayStopBtnCommand = PlaySound;
                    VisibilityOfPlayingStack = Visibility.Collapsed;

                    SoundPlayingTimer.Stop();
                });
            }
        }

        public ICommand DownloadSound
        {
            get
            {
                return new RelayCommand(o =>
                {
                    //Если файл уже скачан, то не качаем второй раз.
                    if (DownloadedFileAbsolutePath != "") return;

                    try
                    {
                        CurrentDownloadPicture = downloadingPicture;
                        VisibilityOfDownloadingStack = Visibility.Visible;
                        string targetFile = Directory.GetCurrentDirectory() + String.Format(@"\{0}{1}", sound.Name, ".wav");
                        DownloadClient.DownloadFileAsync(new Uri(sound.Url), targetFile);
                        DownloadedFileAbsolutePath = targetFile;
                    }
                    catch(Exception anyExcept)
                    {
                        //Выводим окошко с ошибкой
                        CurrentDownloadPicture = downloadPicture;
                        return;
                    }

                    CurrentDownloadPicture = downloadingPicture;

                });
            }
        }

        #endregion
    }
}
