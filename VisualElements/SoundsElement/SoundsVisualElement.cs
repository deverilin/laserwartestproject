﻿using LaserWar.Base;
using LaserWar.DataBase;
using LaserWar.Interfaces;
using LaserWar.VisualElements.DBEditorElement;
using LaserWar.VisualElements.SoundsElement;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace LaserWar.VisualElements
{
    class SoundsVisualElement : BaseViewModel, IVisualElement
    {
        public SoundsVisualElement()
        {
            modelDB = DataBaseSingleton.ModelDB;
            SoundsPlayer = new MediaPlayer();
            ViewSounds = new ObservableCollection<SoundWrapper>();

            DataBaseSingleton.DatabaseUpdated += RefreshSoundsList;

            RefreshSoundsList(this, new EventArgs());
        }

        #region PRIVATE PROPERTIES

        private DataBaseModel modelDB;

        /// <summary>
        /// Проигрыватель звуков. Общий для всех элементов.
        /// </summary>
        private MediaPlayer SoundsPlayer;

        /// <summary>
        /// Список звуков.
        /// </summary>
        private IEnumerable<Sound> sounds;

        #endregion

        #region PUBLIC PROPERTIES

        /// <summary>
        /// Коллекция оберток для сущности звука для работы с представлением.
        /// </summary>
        public ObservableCollection<SoundWrapper> ViewSounds { get; set; }
        
        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Обработчик события обновления базы данных. Загружает звуки из БД при загрузке нового файла JSON.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshSoundsList(object sender, EventArgs e)
        {
            modelDB.Sounds.Load();
            sounds = modelDB.Sounds.Local.ToBindingList();

            ViewSounds.Clear();

            foreach (Sound s in sounds)
            {
                ViewSounds.Add(new SoundWrapper(SoundsPlayer, s));
            }
        }

        #endregion

        #region INTERFACE IMPLEMENTATION

        public UserControl GetControl
        {
            get
            {
                return new SoundsUserControl(this);
            }
        }

        public BitmapImage GetIcon
        {
            get { return new BitmapImage(new Uri("pack://application:,,,/Resources/sounds.png")); }
        }

        #endregion
    }
}
