﻿using LaserWar.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace LaserWar.VisualElements.SoundsElement
{
    /// <summary>
    /// Логика взаимодействия для SoundWrapperPlayingUserControl.xaml
    /// </summary>
    public partial class SoundWrapperPlayingUserControl : UserControl
    {

        public SoundWrapperPlayingUserControl(BaseViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
