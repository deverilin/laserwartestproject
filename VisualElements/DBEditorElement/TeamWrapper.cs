﻿using LaserWar.Base;
using LaserWar.DataBase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace LaserWar.VisualElements.DBEditorElement
{
    class TeamWrapper : BaseViewModel
    {
        public TeamWrapper(Team inTeam)
        {
            team = inTeam;

            Players = new ObservableCollection<PlayerWrapper>();

            foreach(Player p in team.Players)
            {
                Players.Add(new PlayerWrapper(p));
            }
        }

        #region PRIVATE PROPERTIES

        private Team team;
        
        /// <summary>
        /// Видимость таблицы с игроками (спрятана или показана).
        /// </summary>
        private Visibility visibilityOfPlayersList;

        /// <summary>
        /// Положение иконки кнопки выпадающей таблицы игроков.
        /// </summary>
        private double iconDegreesPos;

        #endregion

        #region PUBLIC PROPERTIES

        public ObservableCollection<PlayerWrapper> Players { get; set; }

        public string Name
        {
            get
            {
                return team.Name;
            }

            set
            {
                team.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public Visibility VisibilityOfPlayersList
        {
            get
            {
                return visibilityOfPlayersList;
            }

            set
            {
                visibilityOfPlayersList = value;
                OnPropertyChanged("VisibilityOfPlayersList");
            }
        }

        public double IconDegreesPos
        {
            get
            {
                return iconDegreesPos;
            }

            set
            {
                iconDegreesPos = value;
                OnPropertyChanged("IconDegreesPos");
            }
        }

        #endregion

        #region COMMANDS

        public ICommand ShowPlayersList
        {
            get
            {
                return new RelayCommand(o =>
                {
                    if (visibilityOfPlayersList == Visibility.Collapsed)
                    {
                        VisibilityOfPlayersList = Visibility.Visible;
                    }
                    else
                    {
                        VisibilityOfPlayersList = Visibility.Collapsed;
                    }

                    IconDegreesPos += 180;
                });
            }
        }

        #endregion
    }
}
