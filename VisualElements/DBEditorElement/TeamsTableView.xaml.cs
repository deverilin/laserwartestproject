﻿using LaserWar.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LaserWar.VisualElements.DBEditorElement
{
    /// <summary>
    /// Логика взаимодействия для TeamsTableView.xaml
    /// </summary>
    public partial class TeamsTableView : UserControl
    {
        public TeamsTableView(BaseViewModel ViewModel)
        {
            InitializeComponent();
            DataContext = ViewModel;

            ShowHidePlayersBtn_Click(this, new RoutedEventArgs());
        }

        /// <summary>
        /// Обработчик события нажатия на кнопку. Разворачивает иконку на 180 градусов.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowHidePlayersBtn_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
