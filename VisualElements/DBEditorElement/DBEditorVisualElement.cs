﻿using LaserWar.Base;
using LaserWar.DataBase;
using LaserWar.Interfaces;
using LaserWar.VisualElements.DBEditorElement;
using LaserWar.VisualElements.DBEditorElement.GamesTable;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace LaserWar.VisualElements
{
    class DBEditorVisualElement : BaseViewModel, IVisualElement
    {
        public DBEditorVisualElement()
        {
            modelDB = new DataBaseModel();

            RefreshTables(this, new EventArgs());

            DataBaseSingleton.DatabaseUpdated += RefreshTables;

            formLabel = "ИГРЫ";
            backButtonVisibility = Visibility.Collapsed;
            currentTableView = new GamesTableView(this);
        }

        #region PRIVATE PROPERTIES

        /// <summary>
        /// База данных.
        /// </summary>
        private DataBaseModel modelDB;

        /// <summary>
        /// Текущая форма.
        /// </summary>
        private UserControl currentTableView;

        /// <summary>
        /// Выбранная двойным кликом таблица игр.
        /// </summary>
        private GameWrapper selectedGameTable;

        /// <summary>
        /// Выбранная двойным кликом таблица игроков.
        /// </summary>
        private PlayerWrapper selectedPlayerTable;

        /// <summary>
        /// Заголовок формы. Меняется в зависимости от выбранной игры.
        /// </summary>
        private string formLabel;

        /// <summary>
        /// Видимость кнопки возвращения к списку игр.
        /// </summary>
        private Visibility backButtonVisibility;

#endregion

        #region PUBLIC PROPERTIES

        public UserControl CurrentTableView
        {
            get {
                return currentTableView; }
            set
            {
                currentTableView = value;
                OnPropertyChanged("CurrentTableView");
            }
        }

        public ObservableCollection<GameWrapper> Games { get; set; }
        public ObservableCollection<TeamWrapper> Teams { get; set; }

        public GameWrapper SelectedGameTable
        {
            get
            {
                return selectedGameTable;
            }

            set
            {
                selectedGameTable = value;
                OnPropertyChanged("SelectedGameTable");
            }
        }

        public PlayerWrapper SelectedPlayerTable
        {
            get
            {
                return selectedPlayerTable;
            }

            set
            {
                selectedPlayerTable = value;
                OnPropertyChanged("SelectedPlayerTable");
            }
        }

        public string FormLabel
        {
            get
            {
                return formLabel;
            }

            set
            {
                formLabel = value;
                OnPropertyChanged("FormLabel");
            }
        }

        public Visibility BackButtonVisibility
        {
            get
            {
                return backButtonVisibility;
            }

            set
            {
                backButtonVisibility = value;
                OnPropertyChanged("BackButtonVisibility");
            }
        }

        #endregion

        #region INTERFACE IMPLEMENTATION

        public UserControl GetControl
        {
            get { return new DBEditorUserControl(this); }
        }

        public BitmapImage GetIcon
        {
            get { return new BitmapImage(new Uri("pack://application:,,,/Resources/games.png")); }
        }

        #endregion

        #region EVENTS HANDLERS

        public void RefreshTables(object sender, EventArgs e)
        {
            modelDB.Games.Include("Teams").Load();
            modelDB.Teams.Include("Players").Load();
            IEnumerable<Game> list = modelDB.Games.ToList();

            Games = new ObservableCollection<GameWrapper>();

            foreach (Game g in list)
            {
                Games.Add(new GameWrapper(g));
            }
        }

        #endregion

        #region COMMANDS

        /// <summary>
        /// Двойное нажатие на строке игры.
        /// </summary>
        public ICommand GameClicked
        {
            get
            {
                return new RelayCommand(o =>
                {
                    CurrentTableView = new TeamsTableView(this);
                    FormLabel = SelectedGameTable.Name;
                    BackButtonVisibility = Visibility.Visible;
                });
            }
        }

        /// <summary>
        /// Двойное нажатие на строке игрока.
        /// </summary>
        public ICommand PlayerClicked
        {
            get
            {
                return new RelayCommand(o =>
                {

                });
            }
        }

        /// <summary>
        /// Нажатие кнопки "назад".
        /// </summary>
        public ICommand BackButtonClicked
        {
            get
            {
                return new RelayCommand(o =>
                {
                    CurrentTableView = new GamesTableView(this);
                    FormLabel = "ИГРЫ";
                    BackButtonVisibility = Visibility.Collapsed;
                });
            }
        }

        #endregion
    }
}
