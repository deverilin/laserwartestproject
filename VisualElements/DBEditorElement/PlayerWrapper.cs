﻿using LaserWar.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserWar.VisualElements.DBEditorElement
{
    class PlayerWrapper :  BaseViewModel
    {
        private Player player;

        public PlayerWrapper(Player inPlayer)
        {
            player = inPlayer;
        }

        public string Name
        {
            get
            {
                return player.Name;
            }

            set
            {
                player.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public int Rating
        {
            get
            {
                return player.Rating;
            }

            set
            {
                player.Rating = value;
                OnPropertyChanged("Rating");
            }
        }

        public float Accuracy
        {
            get
            {
                return player.Accuracy;
            }

            set
            {
                player.Accuracy = value;
                OnPropertyChanged("Accuracy");
            }
        }

        public int Shots
        {
            get
            {
                return player.Shots;
            }

            set
            {
                player.Shots = value;
                OnPropertyChanged("Shots");
            }
        }

    }
}
