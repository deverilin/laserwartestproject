﻿using LaserWar.Base;
using LaserWar.DataBase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserWar.VisualElements.DBEditorElement
{
    class GameWrapper : BaseViewModel
    {
        public GameWrapper(Game inGame)
        {
            teams = new ObservableCollection<TeamWrapper>();
            game = inGame;
            foreach(Team t in game.Teams)
            {
                teams.Add(new TeamWrapper(t));
            }
        }

        #region PRIVATE PROPERTIES

        private Game game;

        private ObservableCollection<TeamWrapper> teams;

        #endregion

        #region PUBLIC PROPERTIES

        public string Name
        {
            get
            {
                return game.Name;
            }

            set
            {
                game.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Date
        {
            get
            {
                return new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(game.Date).ToLongDateString();
            }
        }

        public int PlayersCount
        {
            get
            {
                int count = 0;
                foreach(Team t in game.Teams)
                {
                    foreach(Player p in t.Players)
                    {
                        ++count;
                    }
                }

                return count;
            }
        }

        public ObservableCollection<TeamWrapper> Teams
        {
            get
            {
                return teams;
            }

            set
            {
                teams = value;
                OnPropertyChanged("Teams");
            }
        }

        #endregion

    }
}
