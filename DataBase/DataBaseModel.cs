﻿using LaserWar.DataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserWar.VisualElements.DBEditorElement
{
    public class DataBaseModel : DbContext
    {
        public DataBaseModel(): base("DefaultConnection") {}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>()
                .HasMany<Player>(p => p.Players)
                .WithMany(t => t.Teams)
                .Map(pt =>
                {
                    pt.MapLeftKey("TeamId");
                    pt.MapRightKey("PlayerId");
                    pt.ToTable("TeamsPlayers");
                });

            modelBuilder.Entity<Game>()
                .HasMany<Team>(t => t.Teams)
                .WithMany(g => g.Games)
                .Map(gt =>
                {
                    gt.MapLeftKey("GameId");
                    gt.MapRightKey("TeamId");
                    gt.ToTable("GamesTeams");
                });
        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Sound> Sounds { get; set; }

    }
}
