﻿using LaserWar.Base;
using LaserWar.DataBase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserWar.VisualElements.DBEditorElement
{
    public class Game : BaseViewModel
    {
        public int GameId { get; set; }

        private string name;
        private int date;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public int Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
                OnPropertyChanged("DateTimestamp");
            }
        }

        public ICollection<Team> Teams { get; set; }

        public Game()
        {
            Teams = new ObservableCollection<Team>();
        }
    }
}
