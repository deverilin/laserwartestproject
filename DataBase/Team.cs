﻿using LaserWar.Base;
using LaserWar.DataBase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserWar.VisualElements.DBEditorElement
{
    public class Team : BaseViewModel
    {
        public int TeamId { get; set; }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public ICollection<Game> Games { get; set; }
        public ICollection<Player> Players { get; set; }

        public Team()
        {
            Games = new ObservableCollection<Game>();
            Players = new ObservableCollection<Player>();
        }
    }
}
