﻿using LaserWar.Base;
using LaserWar.DataBase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserWar.VisualElements.DBEditorElement
{
    public class Player : BaseViewModel
    {
        private string name;
        private float accuracy;
        private int rating;
        private int shots;

        public int PlayerId { get; set; }

        public ICollection<Team> Teams { get; set; }

        public Player()
        {
            Teams = new ObservableCollection<Team>();
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public float Accuracy
        {
            get
            {
                return accuracy;
            }

            set
            {
                accuracy = value;
                OnPropertyChanged("Accuracy");
            }
        }

        public int Rating
        {
            get
            {
                return rating;
            }

            set
            {
                rating = value;
                OnPropertyChanged("Raiting");
            }
        }

        public int Shots
        {
            get
            {
                return shots;
            }

            set
            {
                shots = value;
                OnPropertyChanged("Shots");
            }
        }
    }
}
