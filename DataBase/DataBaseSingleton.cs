﻿using LaserWar.VisualElements.DBEditorElement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserWar.DataBase
{
    class DataBaseSingleton
    {
        private static DataBaseModel modelDB;
        private static bool isNewFile;

        public static DataBaseModel ModelDB
        {
            get
            {
                if(modelDB == null)
                {
                    modelDB = new DataBaseModel();
                }

                return modelDB;
            }
        }

        public static EventHandler DatabaseUpdated;
    }
}
