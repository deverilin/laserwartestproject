﻿using LaserWar.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserWar.DataBase
{
    public class Sound : BaseViewModel
    {
        public int Id { get; set; }

        private string name;
        private string url;
        private int size;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Url
        {
            get
            {
                return url;
            }

            set
            {
                url = value;
                OnPropertyChanged("Url");
            }
        }

        public int Size
        {
            get
            {
                return size;
            }
            
            set
            {
                size = value;
                OnPropertyChanged("Size");
            }
        }

    }
}
