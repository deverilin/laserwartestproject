﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LaserWar.Resources
{
    /// <summary>
    /// Interaction logic for PleaseWaitWindow.xaml
    /// </summary>
    public partial class PleaseWaitWindow : Window
    {
        public PleaseWaitWindow()
        {
            InitializeComponent();

            Owner = Application.Current.MainWindow;
            Height = Application.Current.MainWindow.ActualHeight;
            Width = Application.Current.MainWindow.ActualWidth - 14;
        }
    }
}
